﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
   public class Message
    {
        public Message(string userName, string msg) {
            this.userName = userName;
            this.msg = msg;
        }
        public string userName { get; set; }
        public string msg { get; set; }
    }
}
